<?php

/**
 * @file
 * Defines the View Style Plugins.
 */

/**
 * Implements hook_views_plugins().
 */
function collageplus_views_plugins() {
  $module_path = drupal_get_path('module', 'collageplus');
  return array(
    'style' => array(
      'collageplus' => array(
        'title' => t('Collageplus'),
        'help' => t('Display the results as Collageplus.'),
        'handler' => 'collageplus_plugin_style_collage',
        'uses row plugin' => TRUE,
        'uses row class' => TRUE,
        'uses grouping' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'path' => $module_path,
        'theme' => 'collageplus_collage',
        'theme path' => $module_path . '/theme',
        'theme file' => 'collageplus.theme.inc',
      ),
    ),
  );
}
