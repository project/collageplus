<?php

/**
 * @file
 * Drush integration for installing CollagePlus Plugin.
 */

/**
 * The collageplus plugin URI.
 */
define('COLLAGEPLUS_DOWNLOAD_URI', 'https://github.com/ed-lea/jquery-collagePlus/archive/master.zip');
define('COLLAGEPLUS_DOWNLOAD_PREFIX', 'collageplus-');

/**
 * Implements hook_drush_command().
 */
function collageplus_drush_command() {
  $items = array();

  $items['collageplus-plugin'] = array(
    'callback' => 'drush_collageplus_plugin',
    'description' => dt('Download and install the collageplus plugin.'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'arguments' => array(
      'path' => dt('Optional. A path where to install the collageplus plugin. If omitted Drush will use the default location.'),
    ),
    'aliases' => array('collageplusplugin'),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function collageplus_drush_help($section) {
  switch ($section) {
    case 'drush:collageplus-plugin':
      return dt('Download and install the collageplus plugin from jacklmoore.com/collageplus, default location is sites/all/libraries.');
  }
}


/**
 * Command to download the collageplus plugin.
 */
function drush_collageplus_plugin() {
  $args = func_get_args();
  if (!empty($args[0])) {
    $path = $args[0];
  }
  else {
    $path = 'sites/all/libraries';
  }

  // Create the path if it does not exist.
  if (!is_dir($path)) {
    drush_op('mkdir', $path);
    drush_log(dt('Directory @path was created', array('@path' => $path)), 'notice');
  }

  // Set the directory to the download location.
  $olddir = getcwd();
  chdir($path);

  // Download the zip archive.
  if ($filepath = drush_download_file(COLLAGEPLUS_DOWNLOAD_URI)) {
    $filename = basename($filepath);
    $dirname = 'jquery-collagePlus-master';
    drush_print_r($filename);
    drush_print_r($dirname);

    // Remove any existing collageplus plugin directory.
    if (is_dir($dirname) || is_dir('jquery-collagePlus-master')) {
      drush_delete_dir($dirname, TRUE);
      drush_delete_dir('collageplus', TRUE);
      drush_log(dt('A existing collageplus plugin was deleted from @path', array('@path' => $path)), 'notice');
    }

    // Decompress the zip archive.
    drush_tarball_extract($filename);

    // Change the directory name to "collageplus" if needed.
    if ($dirname != 'collageplus') {
      drush_move_dir($dirname, 'collageplus', TRUE);
      $dirname = 'collageplus';
    }
  }

  if (is_dir($dirname)) {
    drush_log(dt('collageplus plugin has been installed in @path', array('@path' => $path)), 'success');
  }
  else {
    drush_log(dt('Drush was unable to install the collageplus plugin to @path', array('@path' => $path)), 'error');
  }

  // Set working directory back to the previous working directory.
  chdir($olddir);
}
