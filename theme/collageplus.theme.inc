<?php
/**
 * @file
 * Theming related changes for collage views.
 */

/**
 * Implements template_preprocess_HOOK().
 */
function template_preprocess_collageplus_collage(&$vars) {
  $view = $vars['view'];

  $vars['classes_array'] = array();
  $vars['classes'] = array();

  $vars['collage_html_ids'] = drupal_html_id('views_collage_wrapper');
  $vars['html_container_id'] = drupal_html_id('collageplus_container');
  $vars['collageplus_collage'] = 'collage';

  // Merge view options and default values.
  $vars['options'] = $view->style_plugin->options + array(
    'collageplus_container' => $vars['html_container_id'],
  );
}

/**
 * Implements template_process_HOOK().
 */
function template_process_collageplus_collage(&$vars) {
  $options = (object) (!empty($vars['options']) ? $vars['options'] : array());
  // Sanitize options.
  $options->image_field = drupal_html_class($options->grouping[0]['field']);

  // Add required js and css
  // Don't load javascript unless libraries module is present.
  if (module_exists('libraries')) {

    // Load CollagePlus plugin.
    if ($collage_path = _collageplus_library_path()) {
      drupal_add_js($collage_path);
    }

    // Scripts to remove all the whitespaces.
    if ($removespaces_path = _collageplus_removespaces_library_path()) {
      drupal_add_js($removespaces_path);
    }

    // Script for Captions.
    if ($options->caption) {
      if ($caption_path = _collageplus_caption_library_path()) {
        drupal_add_js($caption_path);
      }
    }

  }
  // Add collage configurations js.
  drupal_add_js(array('collageplus' => array($vars['collageplus_collage'] => $options)), 'setting');
  drupal_add_js(drupal_get_path('module', 'collageplus') . '/js/collageplus.js');
}
