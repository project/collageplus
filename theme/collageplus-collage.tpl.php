<?php
/**
 * @file
 * Template files for collageplus views.
 */
?>

<?php if ($rows): ?>
  <div id="<?php print $html_container_id; ?>">
    <?php foreach ($rows as $id => $row): ?>
      <div class="Image_Wrapper">
        <?php if ($row): ?>
          <?php foreach ($row as $id => $field): ?>
            <?php print $field; ?>
          <?php endforeach; ?>
        <?php endif; ?>
      </div>
    <?php endforeach; ?>
  </div>
<?php endif; ?>
