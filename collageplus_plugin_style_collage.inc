<?php

/**
 * @file
 * Contains the views ui collage style plugin.
 */

/**
 * Style plugin to render each item in ui collage.
 *
 * @ingroup views_style_plugins
 */
class collageplus_plugin_style_collage extends views_plugin_style {

  /**
   * Set Default options.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['row_height'] = array('default' => '400');
    $options['speed'] = array('default' => 'slow');
    $options['effect'] = array('default' => 0);
    $options['direction'] = array('default' => 0);
    $options['allowpartiallastrow'] = array('default' => 0);
    $options['padding'] = array('default' => '10px');
    return $options;
  }

  /**
   * Options form.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['row_height'] = array(
      '#title' => t('Row Height'),
      '#field_suffix' => ' px',
      '#type' => 'textfield',
      '#default_value' => $this->options['row_height'],
      '#description' => t("The ideal height you want your row to be. It won't set it exactly to this as plugin adjusts the row height to get the correct width."),
      '#size' => '20',
    );
    $form['speed'] = array(
      '#title' => t('Fade speed'),
      '#type' => 'textfield',
      '#default_value' => $this->options['speed'],
      '#description' => t('How quickly you want images to fade in once ready. <br />Can be in ms, "slow" or "fast". <br/> This is only used in the default fade in effect. Timing of the other effects is controlled in CSS'),
      '#size' => '20',
    );
    $form['padding'] = array(
      '#title' => t('Padding'),
      '#type' => 'textfield',
      '#default_value' => $this->options['padding'],
      '#description' => t('Define how much padding you want in between your images.'),
      '#size' => '20',
    );
    $form['effect'] = array(
      '#title' => t('Effect'),
      '#field_suffix' => ' px',
      '#type' => 'select',
      '#options' => array(
        'default' => t('Default'),
        'effect-1' => t('Effect-1'),
        'effect-2' => t('Effect-2'),
        'effect-3' => t('Effect-3'),
        'effect-4' => t('Effect-4'),
        'effect-5' => t('Effect-5'),
        'effect-6' => t('Effect-6'),
      ),
      '#default_value' => $this->options['effect'],
      '#description' => t('Which effect you want to use for revealing the images (note CSS3 browsers only). Default is the safest option for supporting older browsers'),
    );
    $form['direction'] = array(
      '#title' => t('Directon'),
      '#multiple_toggle' => '1',
      '#type' => 'select',
      '#default_value' => $this->options['direction'],
      '#options' => array(
        'vertical' => t('Vertical'),
        'horizontal' => t('Horizontal'),
      ),
    );
    $form['allowpartiallastrow'] = array(
      '#title' => t('Last Row - Image Scaling'),
      '#type' => 'select',
      '#options' => array(
        'true' => t('True'),
        'false' => t('False'),
      ),
      '#default_value' => $this->options['allowpartiallastrow'],
      '#description' => t('Allow the images on the last row to scale fill the parent element width.'),
    );
    $form['caption'] = array(
      '#title' => t('Enable Captions'),
      '#type' => 'checkbox',
      '#description' => t('Set the Title field as caption.'),
      '#default_value' => $this->options['caption'],
    );
    // Alter description title.
    $form['grouping']['#description'] = t('Specify image field.');
  }

  /**
   * Render the display in this style.
   */
  public function render() {
    if ($this->uses_row_plugin() && empty($this->row_plugin)) {
      debug('collageplus_plugin_style_collage: Missing row plugin');
      return;
    }

    // Group the rows according to the grouping field, if specified.
    $sets = $this->render_grouping($this->view->result, $this->options['grouping']);

    // Render each group separately and concatenate.  Plugins may override this
    // method if they wish some other way of handling grouping.
    $output = '';
    $uses_row_plugin = $this->uses_row_plugin();
    foreach ($sets as $title => $records) {
      $rows[$title] = array();
      if ($uses_row_plugin) {
        foreach ($records as $row_index => $row) {
          $this->view->row_index = $row_index;
          $rows[$title][$row_index] = $this->row_plugin->render($row);
        }
      }
      else {
        $rows[$title][] = $records;
      }
    }
    unset($this->view->row_index);

    $output .= theme($this->theme_functions(),
      array(
        'view' => $this->view,
        'options' => $this->options,
        'rows' => $rows,
        'title' => '')
      );
    return $output;
  }

  /**
   * Validate style.
   */
  public function validate() {
    $errors = parent::validate();

    // Results must be grouped.
    if (empty($this->options['grouping'])) {
      $errors[] = t('Style @style requires a grouping (image)field.', array('@style' => $this->definition['title']));
    }

    return $errors;
  }

}
