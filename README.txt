DESCRIPTION:
-----------------
This module helps creating in elegant collage image gallery using ColalgePlus
image gallery plugin for jQuery.


SUPPORTED MODULES:
-----------------
  - Colorbox module [https://drupal.org/project/colorbox]
  - Lightbox2 module [https://drupal.org/project/lightbox2]

INSTALLATION:
------------------
 - Download and install Collageplus and Libraries API modules.
 - Downlod CollagePlus library and extract it in libraries directory.
[you can download it from here: http://collageplus.edlea.com]
 - Rename extracted directory to 'collageplus'.

USAGE:
------------------
   - Create a view with image field.
   - Select "Collageplus" display format.
   - Select the image field as grouping field to select image to
      display on collage.
   - Set appropriate format settings for "Collageplus".
   - Clear cache.

DRUSH COMMAND
------------------
	The easiest way to download and install the plugin is via the built in
  Drush command.

		drush collageplusplugin

TROUBLESHOOTING:
------------------
 - Make sure you have set image field as grouping field in format settings.

DEPENDENCIES:
-----------------
Libraries API
Views
CollagePlus library

--
Initial development of this module was sponsored by Fulcrum Worldwide.
