/**
 * @file
 *
 * Javascript to pass views configurations to frontend.
 */

(function ($) {

  Drupal.behaviors.collageplus = {};
  Drupal.behaviors.collageplus.attach = function(context, settings) {
    var image_field = settings.collageplus.collage.image_field,
        container = settings.collageplus.collage.collageplus_container,
        targetHeight = settings.collageplus.collage.row_height,
        fadeSpeed = settings.collageplus.collage.speed,
        effect = settings.collageplus.collage.effect,
        direction = settings.collageplus.collage.direction;
        allowPartialLastRow = settings.collageplus.collage.allowpartiallastrow;
        padding = settings.collageplus.collage.padding;
        captions = settings.collageplus.collage.caption;

        $('#' + container + ' .Image_Wrapper').css("opacity", 0);
        $(".views-field-" + image_field).siblings().hide();
        collage();
        if(captions) {
            $('#' + container).collageCaption();
        }

    // Adds text markup to caption attribute.
    $(".views-field-" + image_field).bind(
        {
            load: function(e) {
                $(this).siblings().addClass('caption-content');
                $(this).parent()
                .attr('data-caption', $(this).siblings().html());
            },
        }
    );
    var resizeTimer = null;

    // This is to adjust collage on resize.
    $(window).bind('resize', function() {

        // Hide all the images until we resize them.
        $('#' + container + ' .Image_Wrapper').css("opacity", 0);

        // Set a timer to re-apply the plugin.
        if (resizeTimer) {
            clearTimeout(resizeTimer);
        }
        resizeTimer = setTimeout(
            collage,
            200
        );
    });

    function collage() {
        $('#' + container).css('padding', padding);
        $('#' + container).removeWhitespace().collagePlus(
            {
                'fadeSpeed'     : fadeSpeed,
                'targetHeight'  : targetHeight,
                'effect'        : effect,
                'direction'     : direction,
                'allowPartialLastRow'     : allowPartialLastRow,
            }
        );
    };
  };
})(jQuery);
